import argparse
import logging


def setupLogger():
    logger = logging.getLogger('myapp')
    hdlr = logging.FileHandler('myapp.log')
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)
    return logger


parser = argparse.ArgumentParser()

parser.add_argument('-a', '--add', dest='names', action='append', help="provides value to save")

args = parser.parse_args()

names = args.names
finalResult = {}
logger = setupLogger()
for name in names:
    splittedName = name.split(":")
    if len(splittedName) != 2:
        # logger.setLevel(logging.WARNING)
        # logger.error("Bad input, receive : " + name)
        raise ValueError("Bad input, expected '-a name:age'")
    if not splittedName[1].isdigit():
        # logger.setLevel(logging.WARNING)
        # logger.error("Bad age, receive : " + splittedName[1])
        raise ValueError("Bad input, expected age as int")
    finalResult[splittedName[0]] = splittedName[1]

logger.info("MIN: {}, MAX: {}".format(min(finalResult.items(), key=lambda x: x[1]),
                                      max(finalResult.items(), key=lambda x: x[1])))
