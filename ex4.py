# # create dict, and save it to the database
# import sqlite3
#
# sql_create_names_table = """CREATE TABLE IF NOT EXISTS userNames (
#                                    id integer PRIMARY KEY,
#                                    firstName text NOT NULL,
#                                    age integer
#                                );"""
# try:
#     sqliteConnection = sqlite3.connect('SQLite_ex4.db')
#     print("connected to sqlite database")
#
#     c = sqliteConnection.cursor()
#     c.execute(sql_create_names_table)
#     c.close()
#
#     values = {'firstName': 'Kamil Żarski', 'age': '24'}
#
#     c = sqliteConnection.cursor()
#     columns = ', '.join(values.keys())
#     placeholders = ', '.join('?' * len(values))
#     sql = 'INSERT INTO userNames ({}) VALUES ({})'.format(columns, placeholders)
#     c.execute(sql, tuple(values.values()))
#     c.close()
#
#     c = sqliteConnection.cursor()
#     c.execute("SELECT * FROM userNames")
#     rows = c.fetchall()
#     for row in rows:
#         print(row)
#     c.close()
#
#
# except sqlite3.Error as error:
#     print("Error while connecting to sqlite:", error)
# finally:
#     if (sqliteConnection):
#         sqliteConnection.close()
#         print("The SQLite connection is closed")

# # sorting  using multiproccessing
# from multiprocessing import Pool
# import numpy as np
#
#
# def createandsort(n):
#     rand = np.random.RandomState(42)
#     a = rand.rand(n)
#     b = a.sort()
#     # print(a)
#     return b
#
#
# s = [10 ** 1 for i in range(0, 3)]
#
# if __name__ == "__main__":
#     pool = Pool(processes=3)
#     pool.map(createandsort, s)

# sum of int list, using reduce
import functools

ages = [11, 3, -5, 6, 22, ]


result = functools.reduce(lambda a, b: a + b, ages)
print(result)
