#!/bin/python3


def isLess(x):
    return x < 80


namesAndAge = {"Kamil Żarski": 24,
               "Jan Nowak": 87,
               "Stefan Batory": 104}

print(list(map(lambda x: x < 80, namesAndAge.values())))
print(list(map(isLess, namesAndAge.values())))

