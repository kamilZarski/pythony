from functools import reduce
import argparse
import json
import socket
import statistics

SERVERHOST = "localhost"
SERVERPORT = 8888
data = {"name": "Client", "sum": 12, "avg": 1, "mean": 1, "mul": 1, "set": {}}
parser = argparse.ArgumentParser()

parser.add_argument('-a', '--add', dest='numbers', action='append', help="provides value to save")
args = parser.parse_args()
numbers = list(map(int, args.numbers[0].split(' ')))
data["sum"] = sum(numbers)
data["avg"] = data["sum"] / len(numbers)
data["mean"] = statistics.mean(numbers)
data["mul"] = reduce((lambda x, y: x * y), numbers)
data["set"] = str(set(numbers))


class MyClient():
    _host = SERVERHOST
    _port = SERVERPORT
    _data = data

    def __init__(self, host, port):
        if host is not None:
            self._host = host
        if port is not None:
            self._port = port
        print("Client::", self._host, self._port, )

    def run(self):
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sock.connect((self._host, int(self._port)))

        jdata = json.dumps(data)

        self._sock.sendall(bytes(jdata, encoding="utf-8"))
        self._data = self._sock.recv(1024)
        if self._data is not None:
            print("\tRECEIVE:", self._data)
        self._sock.close()


myclient = MyClient(SERVERHOST, SERVERPORT)
myclient.run()
