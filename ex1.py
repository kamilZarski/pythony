#!bin/python3

# ex1
# imie nazwisko to lkucz, a value to wiek - sortowanie
# po nazwisku! imie jako 2.
# np Kamil Żarski jako klucz, i  najpierw po nazwisku

if __name__ == '__main__':
    namesAndAge = {"Kamil Żarski": 24,
                   "Jan Nowak": 87,
                   "Stefan Batory": 104}
    print(namesAndAge.items())
    print(sorted(namesAndAge.items(), key=lambda x: str(x[0].split(' ')[1])))
