#!bin/python3
import logging


def setupLogger():
    logger = logging.getLogger('myapp')
    hdlr = logging.FileHandler('myapp.log')
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.WARNING)
    return logger


# decorator ktory przechwytuje daną wejsciowa i ją loguje
# decorator
def logToFile(func):
    def innerFunction(*args, **kwargs):
        # print("jestem w logerze")

        logger = setupLogger()

        logger.error("{},{}".format(args, kwargs))

        func(*args, **kwargs);
        # print("po funkcji")

    return innerFunction


@logToFile
def simpleFuncion(arg1):
    print(arg1)


@logToFile
def simpleFuncion2(arg1, arg3, arg2):
    print(arg1, arg2, arg3)


@logToFile
def simpleFuncion3(arg1, arg3, arg2):
    print(arg1, arg2, arg3)


if __name__ == '__main__':
    simpleFuncion2("t1", 2, 3)
    simpleFuncion3("t1", 2, arg2="args3")

